module Input exposing (copyButton, submitButton, text)

import Css
import Css.Media as Media
import Html.Styled as Html exposing (..)
import Html.Styled.Attributes as Attr


sharedStyles : Css.Style
sharedStyles =
    Css.batch
        [ Css.borderStyle Css.solid
        , Css.borderWidth (Css.px 1)
        , Css.fontSize (Css.px 16)
        , Css.height (Css.px 40)
        , Css.lineHeight (Css.px 30)
        , Css.outline Css.none
        , Css.padding2 (Css.px 4) (Css.px 16)
        , Media.withMedia
            [ Media.only Media.screen [ Media.maxWidth (Css.px 500) ] ]
            [ Css.borderRadius (Css.px 4)
            , Css.width (Css.pct 100)
            ]
        ]


inputStyles : Css.Style
inputStyles =
    Css.batch
        [ Css.borderRadius4 (Css.px 4) (Css.px 0) (Css.px 0) (Css.px 4)
        , Css.borderColor (Css.hex "bbb")
        , Css.boxShadow5 Css.inset (Css.px 0) (Css.px 0) (Css.px 4) (Css.rgba 0 0 0 0.1)
        , Css.focus
            [ Css.borderColor (Css.hex "c92a60")
            , Css.boxShadow5 Css.inset (Css.px 0) (Css.px 0) (Css.px 3) (Css.hex "c92a60")
            ]
        , Css.hover [ Css.borderColor (Css.hex "c92a60") ]
        , Css.flex (Css.int 1)
        , Media.withMedia
            [ Media.only Media.screen [ Media.maxWidth (Css.px 500) ] ]
            [ Css.flex Css.none ]
        , sharedStyles
        ]


buttonStyles : Css.Style
buttonStyles =
    Css.batch
        [ Css.borderRadius4 (Css.px 0) (Css.px 4) (Css.px 4) (Css.px 0)
        , Css.cursor Css.pointer
        , Css.overflow Css.hidden
        , Css.whiteSpace Css.noWrap
        , Media.withMedia
            [ Media.only Media.screen [ Media.maxWidth (Css.px 500) ] ]
            [ Css.marginTop (Css.px 8) ]
        , sharedStyles
        ]


submitButtonStyles : Css.Style
submitButtonStyles =
    Css.batch
        [ buttonStyles
        , Css.backgroundColor (Css.hex "c92a60")
        , Css.borderColor (Css.hex "9d2a60")
        , Css.color (Css.hex "fff")
        , Css.hover
            [ Css.backgroundColor (Css.hex "9d2a60")
            , Css.borderColor (Css.hex "662041")
            ]
        ]


copyButtonStyles : Css.Style
copyButtonStyles =
    Css.batch
        [ buttonStyles
        , Css.backgroundColor (Css.hex "e5e5e5")
        , Css.borderColor (Css.hex "ccc")
        , Css.color (Css.hex "222")
        , Css.hover
            [ Css.backgroundColor (Css.hex "ddd")
            , Css.borderColor (Css.hex "aaa")
            ]
        ]


text : List (Attribute msg) -> List (Html msg) -> Html msg
text attrs content =
    Html.input (Attr.css [ inputStyles ] :: Attr.type_ "text" :: attrs) content


submitButton : List (Attribute msg) -> List (Html msg) -> Html msg
submitButton attrs content =
    Html.button (Attr.css [ submitButtonStyles ] :: Attr.type_ "submit " :: attrs) content


copyButton : List (Attribute msg) -> List (Html msg) -> Html msg
copyButton attrs content =
    Html.button (Attr.css [ copyButtonStyles ] :: attrs) content
