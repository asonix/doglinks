port module ShowPage exposing (Model, Msg, init, subscriptions, update, view)

import Css
import Css.Media as Media
import Html.Styled exposing (..)
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Events
import Input
import Process
import Task


type alias Model =
    { copied : Bool
    , uuid : String
    }


init : String -> Model
init uuid =
    { copied = False
    , uuid = uuid
    }


headerStyles : Css.Style
headerStyles =
    Css.batch
        [ Css.margin (Css.px 0)
        ]


descriptionStyles : Css.Style
descriptionStyles =
    Css.batch []


copyLineStyles : Css.Style
copyLineStyles =
    Css.batch
        [ Css.displayFlex
        , Media.withMedia
            [ Media.only Media.screen [ Media.maxWidth (Css.px 500) ] ]
            [ Css.display Css.block ]
        ]


homeSectionStyles : Css.Style
homeSectionStyles =
    Css.paddingTop (Css.px 10)


homeStyles : Css.Style
homeStyles =
    Css.batch
        [ Css.color (Css.hex "c92a60")
        , Css.active [ Css.color (Css.hex "c92a60") ]
        , Css.focus [ Css.color (Css.hex "c92a60") ]
        , Css.hover [ Css.color (Css.hex "9d2a60") ]
        , Css.visited
            [ Css.color (Css.hex "c92a60")
            , Css.hover [ Css.color (Css.hex "9d2a60") ]
            ]
        ]


view : String -> Model -> Html Msg
view origin model =
    let
        dogLink =
            origin ++ "/link/" ++ model.uuid
    in
    div []
        [ h3 [ Attr.css [ headerStyles ] ] [ text "Doglink created!" ]
        , p [ Attr.css [ descriptionStyles ] ]
            [ text "Here's your Doglink! You can copy this and paste it wherever you might find useful. Maybe on twitter! I'm not your boss." ]
        , div [ Attr.css [ copyLineStyles ] ]
            [ Input.text [ Attr.id "copy-doglink", Attr.value dogLink ] []
            , Input.copyButton [ Events.onClick ClickedCopy ]
                [ if model.copied then
                    text "Copied!"

                  else
                    text "Copy"
                ]
            ]
        , div [ Attr.css [ homeSectionStyles ] ]
            [ a [ Attr.css [ homeStyles ], Attr.href "/" ] [ text "Return Home" ]
            ]
        ]


type Msg
    = ClickedCopy
    | LinkCopied
    | ExpiredCopy


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ClickedCopy ->
            ( model, copyLink () )

        LinkCopied ->
            ( { model | copied = True }, expireCopy )

        ExpiredCopy ->
            ( { model | copied = False }, Cmd.none )


expireCopy : Cmd Msg
expireCopy =
    Task.perform (\_ -> ExpiredCopy) (Process.sleep 3000)


port copyLink : () -> Cmd msg


port linkCopied : (() -> msg) -> Sub msg


subscriptions : Sub Msg
subscriptions =
    linkCopied (\_ -> LinkCopied)
