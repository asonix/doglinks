use actix_governor::{GlobalKeyExtractor, Governor, GovernorConfigBuilder};
use actix_web::{
    body::MessageBody,
    dev::{ServiceRequest, ServiceResponse},
    error::{ErrorBadRequest, ErrorInternalServerError},
    guard,
    http::header::LOCATION,
    middleware::Compress,
    web::{self, Json, Path},
    App, HttpResponse, HttpServer,
};
use actix_web_lab::{
    middleware::{from_fn, Next},
    web::spa,
};
use sled::{Config, Db, Tree};
use std::time::Duration;
use tracing::Span;
use tracing_actix_web::TracingLogger;
use tracing_log::LogTracer;
use tracing_subscriber::{
    filter::Targets, fmt::format::FmtSpan, layer::SubscriberExt, Layer, Registry,
};
use url::Url;
use uuid::Uuid;

#[derive(Clone)]
struct State {
    link_tree: Tree,
    uuid_tree: Tree,
    _db: Db,
}

impl State {
    fn build(db: Db) -> sled::Result<Self> {
        Ok(State {
            link_tree: db.open_tree("doglinks-link-tree")?,
            uuid_tree: db.open_tree("doglinks-uuid-tree")?,
            _db: db,
        })
    }
}

impl State {
    async fn save_link(&self, link: Url) -> sled::Result<Uuid> {
        let link_tree = self.link_tree.clone();
        let uuid_tree = self.uuid_tree.clone();

        web::block(move || {
            let uuid = Uuid::new_v4();

            uuid_tree.insert(uuid.as_bytes(), link.as_str())?;

            let mut url_key = Vec::from(link.as_str().as_bytes());
            url_key.push(0);
            url_key.extend_from_slice(uuid.as_bytes());

            link_tree.insert(url_key, uuid.as_bytes())?;

            Ok(uuid)
        })
        .await
        .expect("db panicked")
    }

    async fn get_link(&self, uuid: Uuid) -> sled::Result<Option<Url>> {
        let uuid_tree = self.uuid_tree.clone();

        web::block(move || {
            Ok(uuid_tree
                .get(uuid.as_bytes())?
                .and_then(|url| String::from_utf8_lossy(&url).parse().ok()))
        })
        .await
        .expect("db panicked")
    }
}

#[derive(serde::Deserialize)]
struct Link {
    url: Url,
}

#[tracing::instrument(skip(state))]
async fn new_link(
    Json(Link { url }): Json<Link>,
    state: web::Data<State>,
) -> actix_web::Result<HttpResponse> {
    let uuid = state
        .save_link(url)
        .await
        .map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(serde_json::json!({ "uuid": uuid })))
}

#[tracing::instrument(skip(state))]
async fn link_to(path: Path<Uuid>, state: web::Data<State>) -> actix_web::Result<HttpResponse> {
    let url = state
        .get_link(path.into_inner())
        .await
        .map_err(ErrorInternalServerError)?;

    if let Some(url) = url {
        Ok(HttpResponse::SeeOther()
            .insert_header((LOCATION, url.to_string()))
            .json(serde_json::json!({ "url": url })))
    } else {
        Ok(HttpResponse::NotFound().finish())
    }
}

#[derive(Debug)]
struct MissingUserAgent;

impl std::fmt::Display for MissingUserAgent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "No User-Agent header provided")
    }
}

impl std::error::Error for MissingUserAgent {}

#[derive(Debug)]
struct DisallowedUserAgent;

impl std::fmt::Display for DisallowedUserAgent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "User-Agent header is not acceptable")
    }
}

impl std::error::Error for DisallowedUserAgent {}

#[tracing::instrument(level = "debug", skip_all, fields(user_agent))]
async fn deny_invalid_agents(
    req: ServiceRequest,
    next: Next<impl MessageBody>,
) -> actix_web::Result<ServiceResponse<impl MessageBody>> {
    let user_agent = req
        .headers()
        .get("user-agent")
        .ok_or_else(|| ErrorBadRequest(MissingUserAgent))?;
    let parsed = user_agent.to_str().map_err(ErrorBadRequest)?;

    Span::current().record("user_agent", parsed);

    if parsed.to_lowercase().contains("twitter") {
        return Err(ErrorBadRequest(DisallowedUserAgent));
    }

    next.call(req).await
}

#[actix_web::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    LogTracer::init()?;

    let targets: Targets = std::env::var("RUST_LOG")
        .unwrap_or_else(|_| "info".into())
        .parse()?;

    let format_layer = tracing_subscriber::fmt::layer()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_filter(targets.clone());

    let subscriber = Registry::default().with(format_layer);

    tracing::subscriber::set_global_default(subscriber)?;

    let db = Config::new().path("./data/sled-0-34").open()?;

    let state = State::build(db)?;

    let limiter_config = GovernorConfigBuilder::const_default()
        .const_period(Duration::from_secs(10))
        .const_burst_size(1)
        .use_headers()
        .finish()
        .expect("Created a valid rate limit config");

    let general_config = GovernorConfigBuilder::default()
        .period(Duration::from_millis(500))
        .burst_size(4)
        .key_extractor(GlobalKeyExtractor)
        .use_headers()
        .finish()
        .expect("Created a valid rate limit config");

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(state.clone()))
            .wrap(from_fn(deny_invalid_agents))
            .wrap(Compress::default())
            .wrap(TracingLogger::default())
            .service(
                web::resource("/link")
                    .guard(guard::Post())
                    .wrap(Governor::new(&limiter_config))
                    .wrap(Governor::new(&general_config))
                    .route(web::post().to(new_link)),
            )
            .service(
                web::resource("/link/{uuid}")
                    .guard(guard::Get())
                    .route(web::get().to(link_to)),
            )
            .service(
                spa()
                    .index_file("./static/index.html")
                    .static_resources_mount("/static")
                    .static_resources_location("./static")
                    .finish(),
            )
    })
    .bind(("0.0.0.0", 8078))?
    .run()
    .await?;

    Ok(())
}
