#!/usr/bin/env bash

pushd client
elm make src/Main.elm --output=../server/static/main.js --optimize
popd

pushd server
cargo build --release
popd
